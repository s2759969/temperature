package nl.utwente.di.temperature;

import java.util.HashMap;
import java.util.Map;

public class TemperatureConverter {
    public double convertTemperature(Double celsiusTemperature) {
        return celsiusTemperature * 1.8 + 32;
    }
}

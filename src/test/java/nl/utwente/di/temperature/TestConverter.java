package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TestConverter {

    @Test
    public void testBook1() {
        TemperatureConverter quoter = new TemperatureConverter();
        double fahTemp = quoter.convertTemperature(0.0);
        Assertions.assertEquals(32.0, fahTemp, 0.0, "Value of 0 Celsius");
    }
}
